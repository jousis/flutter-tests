# Flutter Tests

Flutter project with various tests



<img src="screenshots/android/Screenshot_1616705594.png" width="200">
<img src="screenshots/android/Screenshot_1616705602.png" width="200">  <br/><br/>
<img src="screenshots/android/Screenshot_1616705640.png" width="200">
<img src="screenshots/windows/Clipboard02.png" width="150">

## List of tests
 - Dark / Light theme switching (manualy and platform based)
 - Basic Theming
 - easy_localization
 - flutter_bloc
 - Auto Router / Navigator 2.0
    - Splash initial page that checks/downloads -insert case here- and then navigates
    - Route guards
    - Nested Routes
    - Tabbar in sync with Bottom Navbar
 - WillPopScope (android backbutton override)
 - Modal popup with image filter backdrop (blur)
 - Super responsive centered widget with shrinking plus (v) scolling  
   - Max width limit for big screens plus perfectly centered
   - Responsive and Scalable for medium screens
   - Shrinking to a ridiculous degree on the X-axis and at the same time scrollable on the Y

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
