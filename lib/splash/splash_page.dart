import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:js_flutter_showcase/router/router.gr.dart';
import 'package:js_flutter_showcase/splash/bloc/splash_bloc.dart';

@RoutePage()
class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SplashBloc, SplashState>(
      listener: (context, state) {
        state.when(
          initial: () {},
          loading: () {},
          loadingDone: () {
            context.router.root
              ..popUntilRoot()
              ..replace(const HomeRoute());
          },
        );
      },
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: Center(
              child: StreamBuilder<double>(
                stream: context.read<SplashBloc>().fakeProgress,
                builder: (context, snapshot) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Text('${snapshot.data ?? 0}%'),
                          SizedBox(
                            width: 60,
                            height: 60,
                            child: CircularProgressIndicator(
                              value: (snapshot.data ?? 0) / 100,
                            ),
                          ),
                        ],
                      ),
                      Text('splashPage.loading.${(snapshot.data ?? 0) ~/ 25}'.tr()),
                    ],
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}
