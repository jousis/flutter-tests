import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'splash_bloc.freezed.dart';
part 'splash_event.dart';
part 'splash_state.dart';

@injectable
class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(const SplashState.initial()) {
    on<SplashEvent>(_onSplashEvent);
  }

  final _fakeProgressStreamController = StreamController<double>();
  StreamSink<double> get fakeProgressSink => _fakeProgressStreamController.sink;
  Stream<double> get fakeProgress => _fakeProgressStreamController.stream;

  @override
  Future<void> close() {
    _fakeProgressStreamController.close();
    return super.close();
  }

  FutureOr<void> _onSplashEvent(
    SplashEvent event,
    Emitter<SplashState> emit,
  ) async {
    await event.map(
      started: (e) => _onSplashStarted(e, emit),
      startedLoading: (e) => _onSplashStartedLoading(e, emit),
    );
  }

  FutureOr<void> _onSplashStarted(
    _Started e,
    Emitter<SplashState> emit,
  ) async {}
  FutureOr<void> _onSplashStartedLoading(
    _StartedLoading e,
    Emitter<SplashState> emit,
  ) async {
    for (var i = 1; i < 101; i++) {
      fakeProgressSink.add(i.toDouble());
      await Future.delayed(const Duration(milliseconds: 1));
    }
    emit(const SplashState.loadingDone());
  }

  // @override
  // Stream<SplashState> mapEventToState(
  //   SplashEvent event,
  // ) async* {
  //   yield* event.map(
  //     started: (e) async* {
  //       yield const SplashState.initial();
  //     },
  //     startedLoading: (e) async* {
  //       yield const SplashState.loading();
  //       for (var i = 1; i < 101; i++) {
  //         fakeProgressSink.add(i.toDouble());
  //         await Future.delayed(const Duration(milliseconds: 1));
  //       }
  //       yield const SplashState.loadingDone();
  //     },
  //   );
  // }
}
