import 'package:flutter/material.dart';

class PinBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            PinButton(value: '1'),
            PinButton(value: '2'),
            PinButton(value: '3'),
          ],
        ),
        const Row(
          children: [
            PinButton(value: '4'),
            PinButton(value: '5'),
            PinButton(value: '6'),
          ],
        ),
        const Row(
          children: [
            PinButton(value: '7'),
            PinButton(value: '8'),
            PinButton(value: '9'),
          ],
        ),
        Row(
          children: [
            PinButton(
              value: 'C',
              onPressed: () {},
            ),
            const PinButton(value: '0'),
            PinButton(
              value: 'T',
              onPressed: () {},
              child: const Icon(Icons.fingerprint, size: 20),
            ),
          ],
        ),
      ],
    );
  }
}

class PinButton extends StatelessWidget {
  const PinButton({
    super.key,
    this.onPressed,
    required this.value,
    this.child,
  });

  final VoidCallback? onPressed;
  final String value;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: OutlinedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          ),
          padding: MaterialStateProperty.all(EdgeInsets.zero),
          textStyle: MaterialStateProperty.all(const TextStyle(fontWeight: FontWeight.bold)),
        ),
        onPressed: onPressed ?? () => debugPrint('will add $value to pin...'),
        child: child ?? Text(value),
      ),
    );
  }
}
