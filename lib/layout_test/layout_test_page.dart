import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/router/router.gr.dart';

class LayoutTestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 800),
              child: Column(
                children: [
                  ElevatedButton(
                    onPressed: () => context.router.push(const PinInputRoute()),
                    child: const Text('Responsive Design'),
                  ),
                  ElevatedButton(
                    onPressed: () => context.router.push(const SimpleModalWithBlurBackdropRoute()),
                    child: const Text('Open Modal'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
