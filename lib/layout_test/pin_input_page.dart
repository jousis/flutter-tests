import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/layout_test/widgets/pin_input.dart';
import 'package:js_flutter_showcase/theme/app_theme.dart';

@RoutePage()
class PinInputPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: LayoutBuilder(
            builder: (context, constraints) => Center(
              // height: constraints.maxHeight,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Center(
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: AppTheme.maxComponentWidth),
                          child: FractionallySizedBox(
                            widthFactor: 0.8,
                            child: OutlinedButton(
                              onPressed: () {},
                              child: const Text('Respect my Responsiveness'),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    const SizedBox(height: 20),
                    IgnorePointer(
                      ignoring: false,
                      child: Center(
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: AppTheme.maxComponentWidth),
                          child: FractionallySizedBox(
                            widthFactor: 0.8,
                            child: FittedBox(fit: BoxFit.fitWidth, child: PinBox()),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              context.router.pop();
                            },
                            child: const Text('Pop'),
                          ),
                          const SizedBox(width: 20),
                          ElevatedButton(
                            onPressed: () {
                              context.router.root.popUntilRoot();
                            },
                            child: const Text('Pop to Root of Root Router'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
