import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

@RoutePage()
class SimpleModalWithBlurBackdropPage extends StatelessWidget {
  const SimpleModalWithBlurBackdropPage();

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
      child: LayoutBuilder(
        builder: (context, constraints) => FractionallySizedBox(
          widthFactor: 0.6,
          heightFactor: 0.6,
          child: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 300),
              child: Scaffold(
                backgroundColor: Theme.of(context).primaryColor,
                body: const Center(
                  child: Text('this is a widget with blur backdrop'),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
