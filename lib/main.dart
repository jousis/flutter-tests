import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:js_flutter_showcase/injection.dart';
import 'package:js_flutter_showcase/main_app_widget.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  const isReleaseBuild = bool.fromEnvironment('dart.vm.product');
  await configureDependencies(isReleaseBuild ? Environment.prod : Environment.dev);
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('el'), Locale('en')],
      path: 'assets/translations',
      fallbackLocale: const Locale('el'),
      child: const MainAppWidget(),
    ),
  );
}
