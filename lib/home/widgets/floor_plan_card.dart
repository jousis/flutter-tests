import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/router/router.gr.dart';

class FloorPlanCard extends StatelessWidget {
  const FloorPlanCard();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          const ListTile(
            title: Text('Floor Plan'),
            subtitle: Text('Restaurant floor plan with tables'),
          ),
          const Divider(),
          ElevatedButton(
            onPressed: () => context.router.root.push(const FloorPlanRoute()),
            child: const Text('With Positioned()'),
          ),
          const SizedBox(
            height: 30,
          ),
          ElevatedButton(
            onPressed: () => context.router.root.push(const FloorPlanAltRoute()),
            child: const Text('With Align() - Not working'),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
