import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:js_flutter_showcase/settings/bloc/settings_bloc.dart';

class DarkModeSwitcher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(
          width: 80,
          child: Switch(
            value: context.read<SettingsBloc>().state.darkMode,
            onChanged: (value) => {
              context.read<SettingsBloc>().add(
                    SettingsEvent.darkModeChangeRequested(darkMode: value),
                  ),
            },
          ),
        ),
        context
            .select((SettingsBloc bloc) => bloc.state.darkMode ? const Icon(Icons.nightlight_round) : const Icon(Icons.wb_sunny)),
      ],
    );
  }
}
