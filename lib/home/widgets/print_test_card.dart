// import 'package:flutter/material.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;
// import 'package:printing/printing.dart';

// class PrintTestCard extends StatefulWidget {
//   const PrintTestCard({
//     Key? key,
//   }) : super(key: key);
//   @override
//   _PrintTestCardState createState() => _PrintTestCardState();
// }

// class _PrintTestCardState extends State<PrintTestCard> {
//   Future<void> _doPrint() async {
//     // final result = await Printing.layoutPdf(
//     //   format: PdfPageFormat.roll80,
//     //   onLayout: (PdfPageFormat format) => Printing.convertHtml(
//     //     format: format,
//     //     html: '<html><body><p>Hello!</p></body></html>',
//     //   ),
//     // );
//     final doc = pw.Document();

//     doc.addPage(
//       pw.Page(
//         pageFormat: PdfPageFormat.roll80,
//         build: (pw.Context context) {
//           return pw.Center(
//             child: pw.Text('Hello World'),
//           ); // Center
//         },
//       ),
//     );
//     // Printing.layoutPdf(onLayout: (PdfPageFormat format) async => doc.save());
//     final printers = await Printing.listPrinters();

//     Printing.directPrintPdf(onLayout: (PdfPageFormat format) async => doc.save());
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       child: Column(
//         children: [
//           ListTile(
//             title: Text('homePage.layoutTestTitle'.tr()),
//             subtitle: Text('homePage.layoutTestSubtitle'.tr()),
//           ),
//           const Divider(),
//           ElevatedButton(
//             onPressed: _doPrint,
//             child: Text('generic.go'.tr()),
//           ),
//         ],
//       ),
//     );
//   }
// }
