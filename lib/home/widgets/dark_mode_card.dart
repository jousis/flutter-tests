import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/home/widgets/dark_mode_switcher.dart';

class DarkModeCard extends StatelessWidget {
  const DarkModeCard({
    super.key,
    required this.darkMode,
  });

  final bool darkMode;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Image(
            width: double.infinity,
            height: 130,
            fit: BoxFit.cover,
            image: AssetImage(
              darkMode ? 'assets/images/dark_mode_image.jpg' : 'assets/images/light_mode_image.jpg',
            ),
          ),
          ListTile(
            title: Text('homePage.darkModeTitle'.tr()),
            subtitle: Text('homePage.darkModeSubtitle'.tr()),
          ),
          const Divider(),
          Text('homePage.orUseTheWidget'.tr()),
          DarkModeSwitcher(),
        ],
      ),
    );
  }
}
