import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class LayoutTestCard extends StatelessWidget {
  const LayoutTestCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text('homePage.layoutTestTitle'.tr()),
            subtitle: Text('homePage.layoutTestSubtitle'.tr()),
          ),
          const Divider(),
          // ElevatedButton(
          //   onPressed: () => context.router.root.push(const LayoutTestRouter()),
          //   child: Text('generic.go'.tr()),
          // ),
        ],
      ),
    );
  }
}
