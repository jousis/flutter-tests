import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class RoutingTestCard extends StatefulWidget {
  const RoutingTestCard({
    super.key,
  });

  @override
  _RoutingTestCardState createState() => _RoutingTestCardState();
}

class _RoutingTestCardState extends State<RoutingTestCard> {
  bool _showTopTabbar = true;
  bool _showBottomTabbar = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text('homePage.routingTestTitle'.tr()),
            subtitle: Text('homePage.routingTestSubtitle'.tr()),
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Top Nav Bar'),
              Switch(
                value: _showTopTabbar,
                onChanged: (value) => setState(
                  () => _showTopTabbar = value,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Bottom Nav Bar'),
              Switch(
                value: _showBottomTabbar,
                onChanged: (value) => setState(
                  () => _showBottomTabbar = value,
                ),
              ),
            ],
          ),
          // ElevatedButton(
          //   onPressed: () => AutoRouter.of(context).push(
          //     RoutingTestMainRoute(
          //       showTopTabbar: _showTopTabbar,
          //       showBottomTabbar: _showBottomTabbar,
          //     ),
          //   ),
          //   child: Text('generic.go'.tr()),
          // ),
        ],
      ),
    );
  }
}
