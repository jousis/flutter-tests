// ignore_for_file: deprecated_member_use

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:js_flutter_showcase/home/widgets/dark_mode_card.dart';
import 'package:js_flutter_showcase/home/widgets/floor_plan_card.dart';
import 'package:js_flutter_showcase/home/widgets/layout_test_card.dart';
import 'package:js_flutter_showcase/home/widgets/routing_test_card.dart';
import 'package:js_flutter_showcase/settings/bloc/settings_bloc.dart';

@RoutePage()
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async {
            final result = await showOkCancelAlertDialog(
              title: 'generic.exitDialogTitle'.tr(),
              message: 'generic.exitDialogMessage'.tr(),
              okLabel: 'generic.yesButton'.tr(),
              cancelLabel: 'generic.noButton'.tr(),
              alertStyle: AdaptiveStyle.material,
              fullyCapitalizedForMaterial: false,
              context: context,
            );
            return result == OkCancelResult.ok;
          },
          child: Scaffold(
            body: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Container(
                    constraints: const BoxConstraints(maxWidth: 800),
                    child: ListView(
                      children: [
                        DarkModeCard(darkMode: state.darkMode),
                        const FloorPlanCard(),
                        const RoutingTestCard(),
                        const LayoutTestCard(),
                        // const PrintTestCard(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
