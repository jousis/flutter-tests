// import 'package:auto_route/auto_route.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:js_flutter_showcase/router/router.gr.dart';

// class Tab3Sub1Page extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Column(
//         children: [
//           Card(
//             child: Column(
//               children: [
//                 ListTile(
//                   title: Text('generic.openNewRoutesTitle'.tr()),
//                 ),
//                 const SizedBox(height: 25),
//                 ElevatedButton(
//                   onPressed: () => AutoTabsRouter.of(context)..setActiveIndex(0),
//                   child: const Text('Goto Tab1'),
//                 ),
//                 ElevatedButton(
//                   onPressed: () => AutoTabsRouter.of(context)
//                     ..setActiveIndex(0)
//                     ..innerRouterOf<StackRouter>(Tab1Router.name)?.push(const Tab1Sub2Route()),
//                   child: const Text('Tab1 --> Sub2 Route'),
//                 ),
//                 ElevatedButton(
//                   onPressed: () {
//                     final tabsRouter = AutoTabsRouter.of(context)..setActiveIndex(0);
//                     final stackRouter = tabsRouter.innerRouterOf<StackRouter>(Tab1Router.name);
//                     if (stackRouter != null) {
//                       stackRouter
//                         ..popUntilRoot()
//                         ..push(const Tab1Sub2Route());
//                     }
//                   },
//                   child: const Text('Tab1 --> popUntilRoot --> Sub2 Route'),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
