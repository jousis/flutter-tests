// import 'package:auto_route/auto_route.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:js_flutter_showcase/router/router.gr.dart';

// class PushPagesCard extends StatelessWidget {
//   const PushPagesCard();

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       child: Column(
//         children: [
//           ListTile(
//             title: Text('generic.openNewRoutesTitle'.tr()),
//           ),
//           const SizedBox(height: 25),
//           ElevatedButton(
//             onPressed: () => AutoRouter.of(context).push(const Tab1Sub2Route()),
//             child: const Text('Open Sub 2 Route'),
//           ),
//           const SizedBox(height: 25),
//           ElevatedButton(
//             onPressed: () => AutoRouter.of(context).push(const Tab1Sub3Route()),
//             child: const Text('Open Sub 3 Route'),
//           ),
//         ],
//       ),
//     );
//   }
// }
