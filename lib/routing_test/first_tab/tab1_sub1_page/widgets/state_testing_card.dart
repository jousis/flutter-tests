import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class StateTestingCard extends StatefulWidget {
  const StateTestingCard({
    super.key,
  });

  @override
  _StateTestingCardState createState() => _StateTestingCardState();
}

class _StateTestingCardState extends State<StateTestingCard> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text(
              'generic.someoneHasThatManyPoints'.plural(
                _counter,
                args: ['generic.user'.tr(), _counter.toString()],
              ),
            ),
          ),
          const SizedBox(height: 25),
          ElevatedButton(onPressed: () => setState(() => _counter++), child: Text('generic.addPoints'.tr())),
          const SizedBox(height: 25),
        ],
      ),
    );
  }
}
