import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Card(
      child: Column(
        children: [
          ListTile(
            title: Text('If you re-select an already active tab, it will pop to root.'),
          ),
          SizedBox(height: 25),
        ],
      ),
    );
  }
}
