import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/routing_test/first_tab/tab1_sub1_page/widgets/info_card.dart';
import 'package:js_flutter_showcase/routing_test/first_tab/tab1_sub1_page/widgets/state_testing_card.dart';

@RoutePage()
class Tab1Sub1Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          InfoCard(),
          const StateTestingCard(),
          // const PushPagesCard(),
        ],
      ),
    );
  }
}

// ElevatedButton(
//     onPressed: () => AutoRouter.of(context).push(CashierRoute(counterValue: _counter)), child: const Text('cashier')),
// const SizedBox(height: 25),
// ElevatedButton(onPressed: () => AutoRouter.of(context).push(const ShiftsRoute()), child: const Text('Shifts')),
