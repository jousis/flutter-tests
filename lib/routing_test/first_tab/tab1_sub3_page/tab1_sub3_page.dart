import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class Tab1Sub3Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('tab1 sub 3'),
            const SizedBox(height: 20),
            ElevatedButton(onPressed: () => context.router.pop(), child: const Text('Pop')),
          ],
        ),
      ),
    );
  }
}
