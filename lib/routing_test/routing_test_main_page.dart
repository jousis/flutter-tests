// import 'package:auto_route/auto_route.dart';
// import 'package:flutter/material.dart';
// import 'package:js_flutter_showcase/router/router.gr.dart';

// @RoutePage()
// class RoutingTestMainPage extends StatefulWidget {
//   final bool showTopTabbar;
//   final bool showBottomTabbar;
//   const RoutingTestMainPage({
//     super.key,
//     required this.showTopTabbar,
//     required this.showBottomTabbar,
//   });
//   @override
//   _RoutingTestMainPageState createState() => _RoutingTestMainPageState();
// }

// class _RoutingTestMainPageState extends State<RoutingTestMainPage> with SingleTickerProviderStateMixin {
//   late final TabController _tabController;

//   final List<PageRouteInfo<dynamic>> _routeList = const [
//     Tab1Router(),
//     Tab2Router(),
//     Tab3Router(),
//   ];

//   @override
//   void initState() {
//     super.initState();
//     _tabController = TabController(vsync: this, length: _routeList.length);
//   }

//   @override
//   void dispose() {
//     _tabController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: AutoTabsRouter(
//         routes: _routeList,
//         duration: const Duration(milliseconds: 400),
//         transitionBuilder: (context, child, animation) {
//           final tabsRouter = context.tabsRouter;
//           if (_tabController.index != tabsRouter.activeIndex) {
//             _tabController.index = tabsRouter.activeIndex;
//           }
//           return Scaffold(
//             appBar: widget.showTopTabbar
//                 ? AppBar(
//                     leading: Container(),
//                     flexibleSpace: SafeArea(
//                       child: Center(
//                         child: Container(
//                           constraints: const BoxConstraints(maxWidth: 450),
//                           child: TabBar(
//                             onTap: (index) {
//                               if (tabsRouter.activeIndex != index) {
//                                 tabsRouter.setActiveIndex(index);
//                               } else {
//                                 tabsRouter.stackRouterOfIndex(index)?.popUntilRoot();
//                               }
//                             },
//                             controller: _tabController,
//                             tabs: [
//                               Image(
//                                 image: const AssetImage('assets/images/logo.png'),
//                                 color: tabsRouter.activeIndex != 0
//                                     ? Theme.of(context).tabBarTheme.unselectedLabelColor
//                                     : Theme.of(context).tabBarTheme.labelColor,
//                               ),
//                               const Tab(
//                                 icon: Icon(Icons.message),
//                               ),
//                               const Tab(
//                                 icon: Icon(Icons.fastfood),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//                 : null,
//             body: Center(
//               child: Container(
//                 constraints: const BoxConstraints(maxWidth: 800),
//                 child: FadeTransition(
//                   opacity: animation,
//                   child: child,
//                 ),
//               ),
//             ),
//             bottomNavigationBar: widget.showBottomTabbar ? buildBottomNavigationBar(tabsRouter) : null,
//           );
//         },
//       ),
//     );
//   }

//   BottomNavigationBar buildBottomNavigationBar(TabsRouter tabsRouter) {
//     return BottomNavigationBar(
//       currentIndex: tabsRouter.activeIndex,
//       onTap: (index) {
//         if (tabsRouter.activeIndex != index) {
//           tabsRouter.setActiveIndex(index);
//         } else {
//           tabsRouter.stackRouterOfIndex(index)?.popUntilRoot();
//         }
//       },
//       items: [
//         BottomNavigationBarItem(
//           // backgroundColor: Theme.of(context).backgroundColor,
//           icon: Container(
//             height: 40,
//             child: Image(
//               image: const AssetImage('assets/images/logo.png'),
//               color: tabsRouter.activeIndex != 0
//                   ? Theme.of(context).tabBarTheme.unselectedLabelColor
//                   : Theme.of(context).tabBarTheme.labelColor,
//             ),
//           ),
//           label: 'Home',
//         ),
//         const BottomNavigationBarItem(
//           // backgroundColor: Theme.of(context).backgroundColor.withRed(222),
//           icon: Icon(Icons.list_alt),
//           label: 'Orders',
//         ),
//         const BottomNavigationBarItem(
//           // backgroundColor: Theme.of(context).backgroundColor.withRed(200),
//           icon: Icon(Icons.fiber_new),
//           label: 'Ordering',
//         ),
//       ],
//     );
//   }
// }
