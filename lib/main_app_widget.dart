import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:js_flutter_showcase/injection.dart';
import 'package:js_flutter_showcase/myapp_widget.dart';
import 'package:js_flutter_showcase/settings/bloc/settings_bloc.dart';
import 'package:js_flutter_showcase/splash/bloc/splash_bloc.dart';

class MainAppWidget extends StatelessWidget {
  const MainAppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SplashBloc>(
          create: (context) => getIt()
            ..add(
              const SplashEvent.startedLoading(),
            ),
        ),
        BlocProvider<SettingsBloc>(
          create: (context) => getIt()
            ..add(
              const SettingsEvent.loadSettingsRequested(),
            ),
        ),
      ],
      child: const MyApp(),
    );
  }
}
