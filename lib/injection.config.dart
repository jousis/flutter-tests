// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'router/router.dart' as _i3;
import 'settings/bloc/settings_bloc.dart' as _i6;
import 'settings/settings_repository.dart' as _i4;
import 'splash/bloc/splash_bloc.dart' as _i5;

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  gh.singleton<_i3.AppRouter>(_i3.AppRouter());
  gh.singleton<_i4.SettingsRepository>(_i4.SettingsRepository());
  gh.factory<_i5.SplashBloc>(() => _i5.SplashBloc());
  gh.factory<_i6.SettingsBloc>(
      () => _i6.SettingsBloc(gh<_i4.SettingsRepository>()));
  return getIt;
}
