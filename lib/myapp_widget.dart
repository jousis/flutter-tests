// ignore_for_file: deprecated_member_use

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:js_flutter_showcase/injection.dart';
import 'package:js_flutter_showcase/router/router.dart';
import 'package:js_flutter_showcase/settings/bloc/settings_bloc.dart';
import 'package:js_flutter_showcase/theme/app_theme.dart';

class MyApp extends StatefulWidget {
  const MyApp({
    super.key,
  });

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  // final _appRouter = AppRouter(authGuard: getIt<AuthGuard>(), loginGuard: getIt<LoginGuard>());
  final _appRouter = getIt<AppRouter>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangePlatformBrightness() {
    context.read<SettingsBloc>().add(const SettingsEvent.systemThemeModeChanged());
    super.didChangePlatformBrightness();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: _appRouter.config(
        navigatorObservers: () => [
          // MyObserver(),
        ],
      ),
      // routerDelegate: _appRouter.delegate(initialDeepLink: '/splash'),
      // routeInformationParser: _appRouter.defaultRouteParser(),
      theme: AppTheme.lightTheme,
      darkTheme: AppTheme.darkTheme,
      themeMode: context.select((SettingsBloc bloc) => bloc.state.darkMode ? ThemeMode.dark : ThemeMode.light),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
    );
  }
}
