import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:js_flutter_showcase/injection.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  // generateForDir: [''],
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
Future<void> configureDependencies(String env) async {
  $initGetIt(getIt, environment: env);
  // getIt.registerSingleton<AppRouter>(AppRouter(devAuthGuard: getIt<DevAuthGuard>(), userAuthGuard: getIt<UserAuthGuard>()));
}
