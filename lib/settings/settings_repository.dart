import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:js_flutter_showcase/settings/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

@singleton
class SettingsRepository {
  static const String _settingsElementKey = 'settings';
  Option<Settings> _cachedSettings = none();

  Settings get defaultSettings => const Settings(
        darkMode: false,
        floorPlanBackgroundAssetPath: '',
        tables: [],
      );

  Future<void> initializeSettingsStorage() async {}

  Future<void> closeStorage() async {}

  Future<Settings> _load() async {
    try {
      final instance = await SharedPreferences.getInstance();
      final settingsMapOrNull = instance.getString(_settingsElementKey);
      return optionOf(settingsMapOrNull)
          .map((jsonString) => jsonDecode(jsonString))
          .map((json) => Settings.fromJson(json as Map<String, dynamic>))
          .fold(
            () => defaultSettings,
            (settings) => settings,
          );
    } catch (_) {
      debugPrint('error while loading settings $_settingsElementKey');
      return defaultSettings;
    }
  }

  Future<Settings> read() async {
    return _cachedSettings.fold(
      () async {
        final settings = await _load();
        _cachedSettings = some(settings);
        return settings;
      },
      (settings) => settings,
    );
  }

  Future<void> saveSettings(Settings settings) async {
    return SharedPreferences.getInstance()
        .then((instance) => instance.setString(_settingsElementKey, jsonEncode(settings.toJson())))
        .then((success) => success ? _cachedSettings = some(settings) : {});
  }
}
