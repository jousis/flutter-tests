import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:js_flutter_showcase/settings/settings_repository.dart';
import 'package:js_flutter_showcase/theme/app_theme.dart';

part 'settings_bloc.freezed.dart';
part 'settings_event.dart';
part 'settings_state.dart';

@injectable
class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc(
    this._settingsRepository,
  ) : super(SettingsState.initial()) {
    on<SettingsEvent>(_onSettingsEvent);
  }

  final SettingsRepository _settingsRepository;

  FutureOr<void> _onSettingsEvent(
    SettingsEvent event,
    Emitter<SettingsState> emit,
  ) async {
    await event.map(
      started: (e) => _onSettingsStarted(e, emit),
      loadSettingsRequested: (e) => _onSettingsLoadSettingsRequested(e, emit),
      darkModeChangeRequested: (e) => _onSettingsDarkModeChangeRequested(e, emit),
      systemThemeModeChanged: (e) => _onSettingsSystemThemeModeChanged(e, emit),
    );
  }

  FutureOr<void> _onSettingsStarted(
    _Started e,
    Emitter<SettingsState> emit,
  ) async {}
  FutureOr<void> _onSettingsLoadSettingsRequested(
    _LoadSettingsRequested e,
    Emitter<SettingsState> emit,
  ) async {
    _settingsRepository.initializeSettingsStorage();
    // IRL we will load from settings and then apply
    final settings = await _settingsRepository.read();
    // final darkMode = AppTheme.currentPlatformBrightness == Brightness.dark;
    final themeMode = settings.darkMode ? ThemeMode.dark : ThemeMode.light;
    AppTheme.setStatusBarAndNavBarColors(themeMode);
    emit(state.copyWith(darkMode: settings.darkMode));
  }

  FutureOr<void> _onSettingsDarkModeChangeRequested(
    _DarkModeChangeRequested e,
    Emitter<SettingsState> emit,
  ) async {
    final themeMode = e.darkMode ? ThemeMode.dark : ThemeMode.light;
    final settings = await _settingsRepository.read();
    _settingsRepository.saveSettings(settings.copyWith(darkMode: e.darkMode));
    AppTheme.setStatusBarAndNavBarColors(themeMode);
    emit(state.copyWith(darkMode: e.darkMode));
  }

  FutureOr<void> _onSettingsSystemThemeModeChanged(
    _SystemThemeModeChanged e,
    Emitter<SettingsState> emit,
  ) async {
    final darkMode = AppTheme.currentPlatformBrightness == Brightness.dark;
    final themeMode = darkMode ? ThemeMode.dark : ThemeMode.light;
    final settings = await _settingsRepository.read();
    _settingsRepository.saveSettings(settings.copyWith(darkMode: darkMode));
    AppTheme.setStatusBarAndNavBarColors(themeMode);
    emit(state.copyWith(darkMode: darkMode));
  }

  // @override
  // Stream<SettingsState> mapEventToState(
  //   SettingsEvent event,
  // ) async* {
  //   yield* event.map(
  //     started: (e) async* {},
  //     loadSettingsRequested: (e) async* {
  //       _settingsRepository.initializeSettingsStorage();
  //       // IRL we will load from settings and then apply
  //       final settings = await _settingsRepository.read();
  //       // final darkMode = AppTheme.currentPlatformBrightness == Brightness.dark;
  //       final themeMode = settings.darkMode ? ThemeMode.dark : ThemeMode.light;
  //       AppTheme.setStatusBarAndNavBarColors(themeMode);
  //       yield state.copyWith(darkMode: settings.darkMode);
  //     },
  //     darkModeChangeRequested: (e) async* {
  //       final themeMode = e.darkMode ? ThemeMode.dark : ThemeMode.light;
  //       final settings = await _settingsRepository.read();
  //       _settingsRepository.saveSettings(settings.copyWith(darkMode: e.darkMode));
  //       AppTheme.setStatusBarAndNavBarColors(themeMode);
  //       yield state.copyWith(darkMode: e.darkMode);
  //     },
  //     systemThemeModeChanged: (e) async* {
  //       final darkMode = AppTheme.currentPlatformBrightness == Brightness.dark;
  //       final themeMode = darkMode ? ThemeMode.dark : ThemeMode.light;
  //       final settings = await _settingsRepository.read();
  //       _settingsRepository.saveSettings(settings.copyWith(darkMode: darkMode));
  //       AppTheme.setStatusBarAndNavBarColors(themeMode);
  //       yield state.copyWith(darkMode: darkMode);
  //     },
  //   );
  // }

  @override
  Future<void> close() {
    _settingsRepository.closeStorage();
    return super.close();
  }
}
