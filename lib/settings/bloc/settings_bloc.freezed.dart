// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'settings_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SettingsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() loadSettingsRequested,
    required TResult Function(bool darkMode) darkModeChangeRequested,
    required TResult Function() systemThemeModeChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? loadSettingsRequested,
    TResult? Function(bool darkMode)? darkModeChangeRequested,
    TResult? Function()? systemThemeModeChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? loadSettingsRequested,
    TResult Function(bool darkMode)? darkModeChangeRequested,
    TResult Function()? systemThemeModeChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_LoadSettingsRequested value)
        loadSettingsRequested,
    required TResult Function(_DarkModeChangeRequested value)
        darkModeChangeRequested,
    required TResult Function(_SystemThemeModeChanged value)
        systemThemeModeChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult? Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult? Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsEventCopyWith<$Res> {
  factory $SettingsEventCopyWith(
          SettingsEvent value, $Res Function(SettingsEvent) then) =
      _$SettingsEventCopyWithImpl<$Res, SettingsEvent>;
}

/// @nodoc
class _$SettingsEventCopyWithImpl<$Res, $Val extends SettingsEvent>
    implements $SettingsEventCopyWith<$Res> {
  _$SettingsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$StartedImplCopyWith<$Res> {
  factory _$$StartedImplCopyWith(
          _$StartedImpl value, $Res Function(_$StartedImpl) then) =
      __$$StartedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StartedImplCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res, _$StartedImpl>
    implements _$$StartedImplCopyWith<$Res> {
  __$$StartedImplCopyWithImpl(
      _$StartedImpl _value, $Res Function(_$StartedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$StartedImpl implements _Started {
  const _$StartedImpl();

  @override
  String toString() {
    return 'SettingsEvent.started()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StartedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() loadSettingsRequested,
    required TResult Function(bool darkMode) darkModeChangeRequested,
    required TResult Function() systemThemeModeChanged,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? loadSettingsRequested,
    TResult? Function(bool darkMode)? darkModeChangeRequested,
    TResult? Function()? systemThemeModeChanged,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? loadSettingsRequested,
    TResult Function(bool darkMode)? darkModeChangeRequested,
    TResult Function()? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_LoadSettingsRequested value)
        loadSettingsRequested,
    required TResult Function(_DarkModeChangeRequested value)
        darkModeChangeRequested,
    required TResult Function(_SystemThemeModeChanged value)
        systemThemeModeChanged,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult? Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult? Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements SettingsEvent {
  const factory _Started() = _$StartedImpl;
}

/// @nodoc
abstract class _$$LoadSettingsRequestedImplCopyWith<$Res> {
  factory _$$LoadSettingsRequestedImplCopyWith(
          _$LoadSettingsRequestedImpl value,
          $Res Function(_$LoadSettingsRequestedImpl) then) =
      __$$LoadSettingsRequestedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadSettingsRequestedImplCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res, _$LoadSettingsRequestedImpl>
    implements _$$LoadSettingsRequestedImplCopyWith<$Res> {
  __$$LoadSettingsRequestedImplCopyWithImpl(_$LoadSettingsRequestedImpl _value,
      $Res Function(_$LoadSettingsRequestedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadSettingsRequestedImpl implements _LoadSettingsRequested {
  const _$LoadSettingsRequestedImpl();

  @override
  String toString() {
    return 'SettingsEvent.loadSettingsRequested()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadSettingsRequestedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() loadSettingsRequested,
    required TResult Function(bool darkMode) darkModeChangeRequested,
    required TResult Function() systemThemeModeChanged,
  }) {
    return loadSettingsRequested();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? loadSettingsRequested,
    TResult? Function(bool darkMode)? darkModeChangeRequested,
    TResult? Function()? systemThemeModeChanged,
  }) {
    return loadSettingsRequested?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? loadSettingsRequested,
    TResult Function(bool darkMode)? darkModeChangeRequested,
    TResult Function()? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (loadSettingsRequested != null) {
      return loadSettingsRequested();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_LoadSettingsRequested value)
        loadSettingsRequested,
    required TResult Function(_DarkModeChangeRequested value)
        darkModeChangeRequested,
    required TResult Function(_SystemThemeModeChanged value)
        systemThemeModeChanged,
  }) {
    return loadSettingsRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult? Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult? Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
  }) {
    return loadSettingsRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (loadSettingsRequested != null) {
      return loadSettingsRequested(this);
    }
    return orElse();
  }
}

abstract class _LoadSettingsRequested implements SettingsEvent {
  const factory _LoadSettingsRequested() = _$LoadSettingsRequestedImpl;
}

/// @nodoc
abstract class _$$DarkModeChangeRequestedImplCopyWith<$Res> {
  factory _$$DarkModeChangeRequestedImplCopyWith(
          _$DarkModeChangeRequestedImpl value,
          $Res Function(_$DarkModeChangeRequestedImpl) then) =
      __$$DarkModeChangeRequestedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool darkMode});
}

/// @nodoc
class __$$DarkModeChangeRequestedImplCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res, _$DarkModeChangeRequestedImpl>
    implements _$$DarkModeChangeRequestedImplCopyWith<$Res> {
  __$$DarkModeChangeRequestedImplCopyWithImpl(
      _$DarkModeChangeRequestedImpl _value,
      $Res Function(_$DarkModeChangeRequestedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? darkMode = null,
  }) {
    return _then(_$DarkModeChangeRequestedImpl(
      darkMode: null == darkMode
          ? _value.darkMode
          : darkMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$DarkModeChangeRequestedImpl implements _DarkModeChangeRequested {
  const _$DarkModeChangeRequestedImpl({required this.darkMode});

  @override
  final bool darkMode;

  @override
  String toString() {
    return 'SettingsEvent.darkModeChangeRequested(darkMode: $darkMode)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DarkModeChangeRequestedImpl &&
            (identical(other.darkMode, darkMode) ||
                other.darkMode == darkMode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, darkMode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DarkModeChangeRequestedImplCopyWith<_$DarkModeChangeRequestedImpl>
      get copyWith => __$$DarkModeChangeRequestedImplCopyWithImpl<
          _$DarkModeChangeRequestedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() loadSettingsRequested,
    required TResult Function(bool darkMode) darkModeChangeRequested,
    required TResult Function() systemThemeModeChanged,
  }) {
    return darkModeChangeRequested(darkMode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? loadSettingsRequested,
    TResult? Function(bool darkMode)? darkModeChangeRequested,
    TResult? Function()? systemThemeModeChanged,
  }) {
    return darkModeChangeRequested?.call(darkMode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? loadSettingsRequested,
    TResult Function(bool darkMode)? darkModeChangeRequested,
    TResult Function()? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (darkModeChangeRequested != null) {
      return darkModeChangeRequested(darkMode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_LoadSettingsRequested value)
        loadSettingsRequested,
    required TResult Function(_DarkModeChangeRequested value)
        darkModeChangeRequested,
    required TResult Function(_SystemThemeModeChanged value)
        systemThemeModeChanged,
  }) {
    return darkModeChangeRequested(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult? Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult? Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
  }) {
    return darkModeChangeRequested?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (darkModeChangeRequested != null) {
      return darkModeChangeRequested(this);
    }
    return orElse();
  }
}

abstract class _DarkModeChangeRequested implements SettingsEvent {
  const factory _DarkModeChangeRequested({required final bool darkMode}) =
      _$DarkModeChangeRequestedImpl;

  bool get darkMode;
  @JsonKey(ignore: true)
  _$$DarkModeChangeRequestedImplCopyWith<_$DarkModeChangeRequestedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SystemThemeModeChangedImplCopyWith<$Res> {
  factory _$$SystemThemeModeChangedImplCopyWith(
          _$SystemThemeModeChangedImpl value,
          $Res Function(_$SystemThemeModeChangedImpl) then) =
      __$$SystemThemeModeChangedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SystemThemeModeChangedImplCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res, _$SystemThemeModeChangedImpl>
    implements _$$SystemThemeModeChangedImplCopyWith<$Res> {
  __$$SystemThemeModeChangedImplCopyWithImpl(
      _$SystemThemeModeChangedImpl _value,
      $Res Function(_$SystemThemeModeChangedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SystemThemeModeChangedImpl implements _SystemThemeModeChanged {
  const _$SystemThemeModeChangedImpl();

  @override
  String toString() {
    return 'SettingsEvent.systemThemeModeChanged()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SystemThemeModeChangedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function() loadSettingsRequested,
    required TResult Function(bool darkMode) darkModeChangeRequested,
    required TResult Function() systemThemeModeChanged,
  }) {
    return systemThemeModeChanged();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function()? loadSettingsRequested,
    TResult? Function(bool darkMode)? darkModeChangeRequested,
    TResult? Function()? systemThemeModeChanged,
  }) {
    return systemThemeModeChanged?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function()? loadSettingsRequested,
    TResult Function(bool darkMode)? darkModeChangeRequested,
    TResult Function()? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (systemThemeModeChanged != null) {
      return systemThemeModeChanged();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_LoadSettingsRequested value)
        loadSettingsRequested,
    required TResult Function(_DarkModeChangeRequested value)
        darkModeChangeRequested,
    required TResult Function(_SystemThemeModeChanged value)
        systemThemeModeChanged,
  }) {
    return systemThemeModeChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult? Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult? Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
  }) {
    return systemThemeModeChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_LoadSettingsRequested value)? loadSettingsRequested,
    TResult Function(_DarkModeChangeRequested value)? darkModeChangeRequested,
    TResult Function(_SystemThemeModeChanged value)? systemThemeModeChanged,
    required TResult orElse(),
  }) {
    if (systemThemeModeChanged != null) {
      return systemThemeModeChanged(this);
    }
    return orElse();
  }
}

abstract class _SystemThemeModeChanged implements SettingsEvent {
  const factory _SystemThemeModeChanged() = _$SystemThemeModeChangedImpl;
}

/// @nodoc
mixin _$SettingsState {
  bool get darkMode => throw _privateConstructorUsedError;
  String get locale => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingsStateCopyWith<SettingsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsStateCopyWith<$Res> {
  factory $SettingsStateCopyWith(
          SettingsState value, $Res Function(SettingsState) then) =
      _$SettingsStateCopyWithImpl<$Res, SettingsState>;
  @useResult
  $Res call({bool darkMode, String locale});
}

/// @nodoc
class _$SettingsStateCopyWithImpl<$Res, $Val extends SettingsState>
    implements $SettingsStateCopyWith<$Res> {
  _$SettingsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? darkMode = null,
    Object? locale = null,
  }) {
    return _then(_value.copyWith(
      darkMode: null == darkMode
          ? _value.darkMode
          : darkMode // ignore: cast_nullable_to_non_nullable
              as bool,
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingsStateImplCopyWith<$Res>
    implements $SettingsStateCopyWith<$Res> {
  factory _$$SettingsStateImplCopyWith(
          _$SettingsStateImpl value, $Res Function(_$SettingsStateImpl) then) =
      __$$SettingsStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool darkMode, String locale});
}

/// @nodoc
class __$$SettingsStateImplCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res, _$SettingsStateImpl>
    implements _$$SettingsStateImplCopyWith<$Res> {
  __$$SettingsStateImplCopyWithImpl(
      _$SettingsStateImpl _value, $Res Function(_$SettingsStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? darkMode = null,
    Object? locale = null,
  }) {
    return _then(_$SettingsStateImpl(
      darkMode: null == darkMode
          ? _value.darkMode
          : darkMode // ignore: cast_nullable_to_non_nullable
              as bool,
      locale: null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SettingsStateImpl implements _SettingsState {
  const _$SettingsStateImpl({required this.darkMode, required this.locale});

  @override
  final bool darkMode;
  @override
  final String locale;

  @override
  String toString() {
    return 'SettingsState(darkMode: $darkMode, locale: $locale)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingsStateImpl &&
            (identical(other.darkMode, darkMode) ||
                other.darkMode == darkMode) &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, darkMode, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingsStateImplCopyWith<_$SettingsStateImpl> get copyWith =>
      __$$SettingsStateImplCopyWithImpl<_$SettingsStateImpl>(this, _$identity);
}

abstract class _SettingsState implements SettingsState {
  const factory _SettingsState(
      {required final bool darkMode,
      required final String locale}) = _$SettingsStateImpl;

  @override
  bool get darkMode;
  @override
  String get locale;
  @override
  @JsonKey(ignore: true)
  _$$SettingsStateImplCopyWith<_$SettingsStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
