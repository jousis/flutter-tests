part of 'settings_bloc.dart';

@freezed
class SettingsEvent with _$SettingsEvent {
  const factory SettingsEvent.started() = _Started;

  /// Loads [Settings] from [ISettingsRepository]
  ///
  /// If the user opens the app for the 1st time, [darkMode] of [Settings] will be none()
  /// and the app will set the [darkMode] to system default and save to [Settings].
  /// From then on, [darkMode] will never be overriden at app start.
  const factory SettingsEvent.loadSettingsRequested() = _LoadSettingsRequested;

  /// Overrides the darkMode upon user interraction
  const factory SettingsEvent.darkModeChangeRequested({required bool darkMode}) = _DarkModeChangeRequested;

  /// Sets the [darkMode] to the system option using
  /// ```dart
  /// SchedulerBinding.instance.window.platformBrightness
  /// ```
  const factory SettingsEvent.systemThemeModeChanged() = _SystemThemeModeChanged;
}
