part of 'settings_bloc.dart';

@freezed
class SettingsState with _$SettingsState {
  const factory SettingsState({
    required bool darkMode,
    required String locale,
  }) = _SettingsState;

  factory SettingsState.initial() => const SettingsState(darkMode: false, locale: 'el');
}
