// import 'package:freezed_annotation/freezed_annotation.dart';
// import 'package:js_flutter_showcase/settings/settings.dart';

// part 'settings_dto.freezed.dart';
// part 'settings_dto.g.dart';

// @freezed
// class FloorPlanSettingsDto with _$FloorPlanSettingsDto {
//   const factory FloorPlanSettingsDto({
//     String? backgroundAssetUrl,
//     List<FloorPlanTableDto>? tables,
//   }) = _FloorPlanSettingsDto;

//   factory FloorPlanSettingsDto.fromJson(Map<String, dynamic> json) => _$FloorPlanSettingsDtoFromJson(json);
//   FloorPlanSettings toDomain() => FloorPlanSettings(
//         backgroundAssetUrl: backgroundAssetUrl ?? '',
//         tables: tables?.map((dto) => dto.toDomain()).toList() ?? [],
//       );
// }

// @freezed
// class FloorPlanTableDto with _$FloorPlanTableDto {
//   const factory FloorPlanTableDto({
//     required String id,
//     String? name,
//     double? xPos,
//     double? yPos,
//     double? width,
//     double? height,
//     int? capacity,
//     int? backgroundColor,
//   }) = _FloorPlanTableDto;

//   factory FloorPlanTableDto.fromJson(Map<String, dynamic> json) => _$FloorPlanTableDtoFromJson(json);

//   FloorPlanTable toDomain() => FloorPlanTable(
//         id: id,
//         name: name ?? id,
//         xPos: xPos ?? 0,
//         yPos: yPos ?? 0,
//         width: width ?? 0,
//         height: height ?? 0,
//         capacity: capacity ?? 0,
//         backgroundColor: backgroundColor ?? 0xFFFF9000,
//       );
//   factory FloorPlanTableDto.fromDomain(FloorPlanTable domain) => FloorPlanTableDto(
//         id: domain.id,
//         name: domain.name,
//         xPos: domain.xPos,
//         yPos: domain.yPos,
//         width: domain.width,
//         height: domain.height,
//         capacity: domain.capacity,
//         backgroundColor: domain.backgroundColor,
//       );
// }
