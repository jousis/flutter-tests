// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SettingsImpl _$$SettingsImplFromJson(Map<String, dynamic> json) =>
    _$SettingsImpl(
      darkMode: json['darkMode'] as bool,
      floorPlanBackgroundAssetPath:
          json['floorPlanBackgroundAssetPath'] as String,
      tables: (json['tables'] as List<dynamic>)
          .map((e) => FloorPlanTable.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$SettingsImplToJson(_$SettingsImpl instance) =>
    <String, dynamic>{
      'darkMode': instance.darkMode,
      'floorPlanBackgroundAssetPath': instance.floorPlanBackgroundAssetPath,
      'tables': instance.tables,
    };

_$FloorPlanTableImpl _$$FloorPlanTableImplFromJson(Map<String, dynamic> json) =>
    _$FloorPlanTableImpl(
      id: json['id'] as String,
      name: json['name'] as String,
      xPos: (json['xPos'] as num).toDouble(),
      yPos: (json['yPos'] as num).toDouble(),
      width: (json['width'] as num).toDouble(),
      height: (json['height'] as num).toDouble(),
      capacity: json['capacity'] as int,
      backgroundColor: json['backgroundColor'] as int,
    );

Map<String, dynamic> _$$FloorPlanTableImplToJson(
        _$FloorPlanTableImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'xPos': instance.xPos,
      'yPos': instance.yPos,
      'width': instance.width,
      'height': instance.height,
      'capacity': instance.capacity,
      'backgroundColor': instance.backgroundColor,
    };
