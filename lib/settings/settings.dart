import 'package:freezed_annotation/freezed_annotation.dart';

part 'settings.freezed.dart';
part 'settings.g.dart';

@freezed
class Settings with _$Settings {
  const factory Settings({
    required bool darkMode,
    required String floorPlanBackgroundAssetPath,
    required List<FloorPlanTable> tables,
  }) = _Settings;

  factory Settings.fromJson(Map<String, dynamic> json) => _$SettingsFromJson(json);
}

@freezed
class FloorPlanTable with _$FloorPlanTable {
  const factory FloorPlanTable({
    required String id,
    required String name,
    required double xPos,
    required double yPos,
    required double width,
    required double height,
    required int capacity,
    required int backgroundColor,
  }) = _FloorPlanTable;

  factory FloorPlanTable.fromJson(Map<String, dynamic> json) => _$FloorPlanTableFromJson(json);
}
