// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'settings.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Settings _$SettingsFromJson(Map<String, dynamic> json) {
  return _Settings.fromJson(json);
}

/// @nodoc
mixin _$Settings {
  bool get darkMode => throw _privateConstructorUsedError;
  String get floorPlanBackgroundAssetPath => throw _privateConstructorUsedError;
  List<FloorPlanTable> get tables => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SettingsCopyWith<Settings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsCopyWith<$Res> {
  factory $SettingsCopyWith(Settings value, $Res Function(Settings) then) =
      _$SettingsCopyWithImpl<$Res, Settings>;
  @useResult
  $Res call(
      {bool darkMode,
      String floorPlanBackgroundAssetPath,
      List<FloorPlanTable> tables});
}

/// @nodoc
class _$SettingsCopyWithImpl<$Res, $Val extends Settings>
    implements $SettingsCopyWith<$Res> {
  _$SettingsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? darkMode = null,
    Object? floorPlanBackgroundAssetPath = null,
    Object? tables = null,
  }) {
    return _then(_value.copyWith(
      darkMode: null == darkMode
          ? _value.darkMode
          : darkMode // ignore: cast_nullable_to_non_nullable
              as bool,
      floorPlanBackgroundAssetPath: null == floorPlanBackgroundAssetPath
          ? _value.floorPlanBackgroundAssetPath
          : floorPlanBackgroundAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      tables: null == tables
          ? _value.tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<FloorPlanTable>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SettingsImplCopyWith<$Res>
    implements $SettingsCopyWith<$Res> {
  factory _$$SettingsImplCopyWith(
          _$SettingsImpl value, $Res Function(_$SettingsImpl) then) =
      __$$SettingsImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool darkMode,
      String floorPlanBackgroundAssetPath,
      List<FloorPlanTable> tables});
}

/// @nodoc
class __$$SettingsImplCopyWithImpl<$Res>
    extends _$SettingsCopyWithImpl<$Res, _$SettingsImpl>
    implements _$$SettingsImplCopyWith<$Res> {
  __$$SettingsImplCopyWithImpl(
      _$SettingsImpl _value, $Res Function(_$SettingsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? darkMode = null,
    Object? floorPlanBackgroundAssetPath = null,
    Object? tables = null,
  }) {
    return _then(_$SettingsImpl(
      darkMode: null == darkMode
          ? _value.darkMode
          : darkMode // ignore: cast_nullable_to_non_nullable
              as bool,
      floorPlanBackgroundAssetPath: null == floorPlanBackgroundAssetPath
          ? _value.floorPlanBackgroundAssetPath
          : floorPlanBackgroundAssetPath // ignore: cast_nullable_to_non_nullable
              as String,
      tables: null == tables
          ? _value._tables
          : tables // ignore: cast_nullable_to_non_nullable
              as List<FloorPlanTable>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SettingsImpl implements _Settings {
  const _$SettingsImpl(
      {required this.darkMode,
      required this.floorPlanBackgroundAssetPath,
      required final List<FloorPlanTable> tables})
      : _tables = tables;

  factory _$SettingsImpl.fromJson(Map<String, dynamic> json) =>
      _$$SettingsImplFromJson(json);

  @override
  final bool darkMode;
  @override
  final String floorPlanBackgroundAssetPath;
  final List<FloorPlanTable> _tables;
  @override
  List<FloorPlanTable> get tables {
    if (_tables is EqualUnmodifiableListView) return _tables;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_tables);
  }

  @override
  String toString() {
    return 'Settings(darkMode: $darkMode, floorPlanBackgroundAssetPath: $floorPlanBackgroundAssetPath, tables: $tables)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingsImpl &&
            (identical(other.darkMode, darkMode) ||
                other.darkMode == darkMode) &&
            (identical(other.floorPlanBackgroundAssetPath,
                    floorPlanBackgroundAssetPath) ||
                other.floorPlanBackgroundAssetPath ==
                    floorPlanBackgroundAssetPath) &&
            const DeepCollectionEquality().equals(other._tables, _tables));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      darkMode,
      floorPlanBackgroundAssetPath,
      const DeepCollectionEquality().hash(_tables));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SettingsImplCopyWith<_$SettingsImpl> get copyWith =>
      __$$SettingsImplCopyWithImpl<_$SettingsImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SettingsImplToJson(
      this,
    );
  }
}

abstract class _Settings implements Settings {
  const factory _Settings(
      {required final bool darkMode,
      required final String floorPlanBackgroundAssetPath,
      required final List<FloorPlanTable> tables}) = _$SettingsImpl;

  factory _Settings.fromJson(Map<String, dynamic> json) =
      _$SettingsImpl.fromJson;

  @override
  bool get darkMode;
  @override
  String get floorPlanBackgroundAssetPath;
  @override
  List<FloorPlanTable> get tables;
  @override
  @JsonKey(ignore: true)
  _$$SettingsImplCopyWith<_$SettingsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

FloorPlanTable _$FloorPlanTableFromJson(Map<String, dynamic> json) {
  return _FloorPlanTable.fromJson(json);
}

/// @nodoc
mixin _$FloorPlanTable {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  double get xPos => throw _privateConstructorUsedError;
  double get yPos => throw _privateConstructorUsedError;
  double get width => throw _privateConstructorUsedError;
  double get height => throw _privateConstructorUsedError;
  int get capacity => throw _privateConstructorUsedError;
  int get backgroundColor => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FloorPlanTableCopyWith<FloorPlanTable> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FloorPlanTableCopyWith<$Res> {
  factory $FloorPlanTableCopyWith(
          FloorPlanTable value, $Res Function(FloorPlanTable) then) =
      _$FloorPlanTableCopyWithImpl<$Res, FloorPlanTable>;
  @useResult
  $Res call(
      {String id,
      String name,
      double xPos,
      double yPos,
      double width,
      double height,
      int capacity,
      int backgroundColor});
}

/// @nodoc
class _$FloorPlanTableCopyWithImpl<$Res, $Val extends FloorPlanTable>
    implements $FloorPlanTableCopyWith<$Res> {
  _$FloorPlanTableCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? xPos = null,
    Object? yPos = null,
    Object? width = null,
    Object? height = null,
    Object? capacity = null,
    Object? backgroundColor = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      xPos: null == xPos
          ? _value.xPos
          : xPos // ignore: cast_nullable_to_non_nullable
              as double,
      yPos: null == yPos
          ? _value.yPos
          : yPos // ignore: cast_nullable_to_non_nullable
              as double,
      width: null == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as double,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as double,
      capacity: null == capacity
          ? _value.capacity
          : capacity // ignore: cast_nullable_to_non_nullable
              as int,
      backgroundColor: null == backgroundColor
          ? _value.backgroundColor
          : backgroundColor // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FloorPlanTableImplCopyWith<$Res>
    implements $FloorPlanTableCopyWith<$Res> {
  factory _$$FloorPlanTableImplCopyWith(_$FloorPlanTableImpl value,
          $Res Function(_$FloorPlanTableImpl) then) =
      __$$FloorPlanTableImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String name,
      double xPos,
      double yPos,
      double width,
      double height,
      int capacity,
      int backgroundColor});
}

/// @nodoc
class __$$FloorPlanTableImplCopyWithImpl<$Res>
    extends _$FloorPlanTableCopyWithImpl<$Res, _$FloorPlanTableImpl>
    implements _$$FloorPlanTableImplCopyWith<$Res> {
  __$$FloorPlanTableImplCopyWithImpl(
      _$FloorPlanTableImpl _value, $Res Function(_$FloorPlanTableImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? xPos = null,
    Object? yPos = null,
    Object? width = null,
    Object? height = null,
    Object? capacity = null,
    Object? backgroundColor = null,
  }) {
    return _then(_$FloorPlanTableImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      xPos: null == xPos
          ? _value.xPos
          : xPos // ignore: cast_nullable_to_non_nullable
              as double,
      yPos: null == yPos
          ? _value.yPos
          : yPos // ignore: cast_nullable_to_non_nullable
              as double,
      width: null == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as double,
      height: null == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as double,
      capacity: null == capacity
          ? _value.capacity
          : capacity // ignore: cast_nullable_to_non_nullable
              as int,
      backgroundColor: null == backgroundColor
          ? _value.backgroundColor
          : backgroundColor // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$FloorPlanTableImpl implements _FloorPlanTable {
  const _$FloorPlanTableImpl(
      {required this.id,
      required this.name,
      required this.xPos,
      required this.yPos,
      required this.width,
      required this.height,
      required this.capacity,
      required this.backgroundColor});

  factory _$FloorPlanTableImpl.fromJson(Map<String, dynamic> json) =>
      _$$FloorPlanTableImplFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final double xPos;
  @override
  final double yPos;
  @override
  final double width;
  @override
  final double height;
  @override
  final int capacity;
  @override
  final int backgroundColor;

  @override
  String toString() {
    return 'FloorPlanTable(id: $id, name: $name, xPos: $xPos, yPos: $yPos, width: $width, height: $height, capacity: $capacity, backgroundColor: $backgroundColor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FloorPlanTableImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.xPos, xPos) || other.xPos == xPos) &&
            (identical(other.yPos, yPos) || other.yPos == yPos) &&
            (identical(other.width, width) || other.width == width) &&
            (identical(other.height, height) || other.height == height) &&
            (identical(other.capacity, capacity) ||
                other.capacity == capacity) &&
            (identical(other.backgroundColor, backgroundColor) ||
                other.backgroundColor == backgroundColor));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name, xPos, yPos, width,
      height, capacity, backgroundColor);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FloorPlanTableImplCopyWith<_$FloorPlanTableImpl> get copyWith =>
      __$$FloorPlanTableImplCopyWithImpl<_$FloorPlanTableImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$FloorPlanTableImplToJson(
      this,
    );
  }
}

abstract class _FloorPlanTable implements FloorPlanTable {
  const factory _FloorPlanTable(
      {required final String id,
      required final String name,
      required final double xPos,
      required final double yPos,
      required final double width,
      required final double height,
      required final int capacity,
      required final int backgroundColor}) = _$FloorPlanTableImpl;

  factory _FloorPlanTable.fromJson(Map<String, dynamic> json) =
      _$FloorPlanTableImpl.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  double get xPos;
  @override
  double get yPos;
  @override
  double get width;
  @override
  double get height;
  @override
  int get capacity;
  @override
  int get backgroundColor;
  @override
  @JsonKey(ignore: true)
  _$$FloorPlanTableImplCopyWith<_$FloorPlanTableImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
