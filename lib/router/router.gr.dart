// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i8;
import 'package:js_flutter_showcase/floor_plan/floor_plan_alt_page.dart' as _i1;
import 'package:js_flutter_showcase/floor_plan/floor_plan_page.dart' as _i2;
import 'package:js_flutter_showcase/home/home_page.dart' as _i3;
import 'package:js_flutter_showcase/layout_test/blur_backdrop_page.dart' as _i5;
import 'package:js_flutter_showcase/layout_test/pin_input_page.dart' as _i4;
import 'package:js_flutter_showcase/routing_test/first_tab/tab1_sub1_page/tab1_sub1_page.dart'
    as _i7;
import 'package:js_flutter_showcase/splash/splash_page.dart' as _i6;

abstract class $AppRouter extends _i8.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i8.PageFactory> pagesMap = {
    FloorPlanAltRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.FloorPlanAltPage(),
      );
    },
    FloorPlanRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i2.FloorPlanPage(),
      );
    },
    HomeRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.HomePage(),
      );
    },
    PinInputRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.PinInputPage(),
      );
    },
    SimpleModalWithBlurBackdropRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.SimpleModalWithBlurBackdropPage(),
      );
    },
    SplashRoute.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.SplashPage(),
      );
    },
    Tab1Sub1Route.name: (routeData) {
      return _i8.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i7.Tab1Sub1Page(),
      );
    },
  };
}

/// generated route for
/// [_i1.FloorPlanAltPage]
class FloorPlanAltRoute extends _i8.PageRouteInfo<void> {
  const FloorPlanAltRoute({List<_i8.PageRouteInfo>? children})
      : super(
          FloorPlanAltRoute.name,
          initialChildren: children,
        );

  static const String name = 'FloorPlanAltRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i2.FloorPlanPage]
class FloorPlanRoute extends _i8.PageRouteInfo<void> {
  const FloorPlanRoute({List<_i8.PageRouteInfo>? children})
      : super(
          FloorPlanRoute.name,
          initialChildren: children,
        );

  static const String name = 'FloorPlanRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i3.HomePage]
class HomeRoute extends _i8.PageRouteInfo<void> {
  const HomeRoute({List<_i8.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i4.PinInputPage]
class PinInputRoute extends _i8.PageRouteInfo<void> {
  const PinInputRoute({List<_i8.PageRouteInfo>? children})
      : super(
          PinInputRoute.name,
          initialChildren: children,
        );

  static const String name = 'PinInputRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i5.SimpleModalWithBlurBackdropPage]
class SimpleModalWithBlurBackdropRoute extends _i8.PageRouteInfo<void> {
  const SimpleModalWithBlurBackdropRoute({List<_i8.PageRouteInfo>? children})
      : super(
          SimpleModalWithBlurBackdropRoute.name,
          initialChildren: children,
        );

  static const String name = 'SimpleModalWithBlurBackdropRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i6.SplashPage]
class SplashRoute extends _i8.PageRouteInfo<void> {
  const SplashRoute({List<_i8.PageRouteInfo>? children})
      : super(
          SplashRoute.name,
          initialChildren: children,
        );

  static const String name = 'SplashRoute';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}

/// generated route for
/// [_i7.Tab1Sub1Page]
class Tab1Sub1Route extends _i8.PageRouteInfo<void> {
  const Tab1Sub1Route({List<_i8.PageRouteInfo>? children})
      : super(
          Tab1Sub1Route.name,
          initialChildren: children,
        );

  static const String name = 'Tab1Sub1Route';

  static const _i8.PageInfo<void> page = _i8.PageInfo<void>(name);
}
