import 'package:auto_route/auto_route.dart';
import 'package:injectable/injectable.dart';
import 'package:js_flutter_showcase/router/router.gr.dart';

@Singleton()
@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
)
class AppRouter extends $AppRouter {
  // late final AuthGuard _authGuard;
  // late final LoginGuard _loginGuard;
  AppRouter(
      // this._authGuard,
      // this._loginGuard,
      )
      : super();

  @override
  List<AutoRoute> get routes => <AutoRoute>[
        AutoRoute(path: '/splash', page: SplashRoute.page, initial: true),
        AutoRoute(path: '/', page: HomeRoute.page),
        AutoRoute(path: '/floor-plan', page: FloorPlanRoute.page),
        AutoRoute(path: '/floor-plan-alt', page: FloorPlanAltRoute.page),
        // CustomRoute(
        //   path: '/layout-test',
        //   name: 'LayoutTestRouter',
        //   page: EmptyRouterPage,
        //   durationInMilliseconds: 300,
        //   reverseDurationInMilliseconds: 200,
        //   transitionsBuilder: TransitionsBuilders.slideBottom,
        //   children: [
        //     CustomRoute(
        //       path: '',
        //       page: LayoutTestPage,
        //       durationInMilliseconds: 300,
        //       reverseDurationInMilliseconds: 200,
        //       transitionsBuilder: TransitionsBuilders.slideLeft,
        //     ),
        //     CustomRoute(
        //       path: 'backdrop',
        //       page: SimpleModalWithBlurBackdropRoute.page,
        //       opaque: false,
        //       fullscreenDialog: true,
        //       durationInMilliseconds: 300,
        //       reverseDurationInMilliseconds: 200,
        //       transitionsBuilder: TransitionsBuilders.zoomIn,
        //     ),
        //     // CustomRoute(
        //     //   path: 'pin',
        //     //   page: PinInputRoute.page,
        //     //   durationInMilliseconds: 300,
        //     //   reverseDurationInMilliseconds: 200,
        //     //   transitionsBuilder: TransitionsBuilders.slideLeft,
        //     // ),
        //     RedirectRoute(path: '*', redirectTo: ''),
        //   ],
        // ),
        // CustomRoute(
        //   path: '/routing-test',
        //   usesTabsRouter: true,
        //   page: RoutingTestMainRoute.page,
        //   guards: const [_authGuard, _loginGuard],
        //   durationInMilliseconds: 300,
        //   reverseDurationInMilliseconds: 200,
        //   transitionsBuilder: TransitionsBuilders.slideBottom,
        //   children: [
        //     AutoRoute(
        //       path: 'tab1',
        //       name: 'Tab1Router',
        //       page: EmptyRouterPage,
        //       children: [
        //         CustomRoute(
        //           path: '',
        //           page: Tab1Sub1Page,
        //           transitionsBuilder: TransitionsBuilders.slideLeft,
        //           durationInMilliseconds: 200,
        //           reverseDurationInMilliseconds: 200,
        //         ),
        //         CustomRoute(
        //           path: 'sub2',
        //           page: Tab1Sub2Page,
        //           transitionsBuilder: TransitionsBuilders.slideLeft,
        //           durationInMilliseconds: 200,
        //           reverseDurationInMilliseconds: 200,
        //         ),
        //         CustomRoute(
        //           path: 'sub3',
        //           page: Tab1Sub3Page,
        //           transitionsBuilder: TransitionsBuilders.slideLeft,
        //           durationInMilliseconds: 200,
        //           reverseDurationInMilliseconds: 200,
        //         ),
        //         RedirectRoute(path: '*', redirectTo: ''),
        //       ],
        //     ),
        //     AutoRoute(
        //       path: 'tab2',
        //       name: 'Tab2Router',
        //       page: EmptyRouterPage,
        //       children: [
        //         AutoRoute(path: '', page: Tab2Sub1Page),
        //         RedirectRoute(path: '*', redirectTo: ''),
        //       ],
        //     ),
        //     AutoRoute(
        //       path: 'tab3',
        //       name: 'Tab3Router',
        //       page: EmptyRouterPage,
        //       children: [
        //         AutoRoute(path: '', page: Tab3Sub1Page),
        //         RedirectRoute(path: '*', redirectTo: ''),
        //       ],
        //     ),
        //   ],
        // ),
      ];
}

// @MaterialAutoRouter(
//   replaceInRouteName: 'Page,Route', //This will replace the Page part of our widget name with a Route
//   routes: <AutoRoute>[
//     AutoRoute(path: '/splash', page: SplashPage, initial: true),
//     AutoRoute(path: '/', page: HomePage),
//     AutoRoute(path: '/floor-plan', page: FloorPlanPage),
//     AutoRoute(path: '/floor-plan-alt', page: FloorPlanAltPage),
//     CustomRoute(
//       path: '/layout-test',
//       name: 'LayoutTestRouter',
//       page: EmptyRouterPage,
//       durationInMilliseconds: 300,
//       reverseDurationInMilliseconds: 200,
//       transitionsBuilder: TransitionsBuilders.slideBottom,
//       children: [
//         CustomRoute(
//           path: '',
//           page: LayoutTestPage,
//           durationInMilliseconds: 300,
//           reverseDurationInMilliseconds: 200,
//           transitionsBuilder: TransitionsBuilders.slideLeft,
//         ),
//         CustomRoute(
//           path: 'backdrop',
//           page: SimpleModalWithBlurBackdropPage,
//           opaque: false,
//           fullscreenDialog: true,
//           durationInMilliseconds: 300,
//           reverseDurationInMilliseconds: 200,
//           transitionsBuilder: TransitionsBuilders.zoomIn,
//         ),
//         CustomRoute(
//           path: 'pin',
//           page: PinInputPage,
//           durationInMilliseconds: 300,
//           reverseDurationInMilliseconds: 200,
//           transitionsBuilder: TransitionsBuilders.slideLeft,
//         ),
//         RedirectRoute(path: '*', redirectTo: ''),
//       ],
//     ),
//     CustomRoute(
//       path: '/routing-test',
//       usesTabsRouter: true,
//       page: RoutingTestMainPage,
//       guards: [AuthGuard, LoginGuard],
//       durationInMilliseconds: 300,
//       reverseDurationInMilliseconds: 200,
//       transitionsBuilder: TransitionsBuilders.slideBottom,
//       children: [
//         AutoRoute(
//           path: 'tab1',
//           name: 'Tab1Router',
//           page: EmptyRouterPage,
//           children: [
//             CustomRoute(
//               path: '',
//               page: Tab1Sub1Page,
//               transitionsBuilder: TransitionsBuilders.slideLeft,
//               durationInMilliseconds: 200,
//               reverseDurationInMilliseconds: 200,
//             ),
//             CustomRoute(
//               path: 'sub2',
//               page: Tab1Sub2Page,
//               transitionsBuilder: TransitionsBuilders.slideLeft,
//               durationInMilliseconds: 200,
//               reverseDurationInMilliseconds: 200,
//             ),
//             CustomRoute(
//               path: 'sub3',
//               page: Tab1Sub3Page,
//               transitionsBuilder: TransitionsBuilders.slideLeft,
//               durationInMilliseconds: 200,
//               reverseDurationInMilliseconds: 200,
//             ),
//             RedirectRoute(path: '*', redirectTo: ''),
//           ],
//         ),
//         AutoRoute(
//           path: 'tab2',
//           name: 'Tab2Router',
//           page: EmptyRouterPage,
//           children: [
//             AutoRoute(path: '', page: Tab2Sub1Page),
//             RedirectRoute(path: '*', redirectTo: ''),
//           ],
//         ),
//         AutoRoute(
//           path: 'tab3',
//           name: 'Tab3Router',
//           page: EmptyRouterPage,
//           children: [
//             AutoRoute(path: '', page: Tab3Sub1Page),
//             RedirectRoute(path: '*', redirectTo: ''),
//           ],
//         ),
//       ],
//     ),
//   ],
// )
// class $AppRouter {}
