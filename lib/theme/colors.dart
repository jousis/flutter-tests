import 'package:flutter/material.dart';

const myAppOrange = 0xFFFA8C1E;
const myAppPink = 0xFFAD1457;

const MaterialColor myAppMaterialColor = MaterialColor(myAppPink, pinkColorSwatch);

const Map<int, Color> orangeColorSwatch = {
  50: Color.fromRGBO(250, 140, 30, .1),
  100: Color.fromRGBO(250, 140, 30, .2),
  200: Color.fromRGBO(250, 140, 30, .3),
  300: Color.fromRGBO(250, 140, 30, .4),
  400: Color.fromRGBO(250, 140, 30, .5),
  500: Color.fromRGBO(250, 140, 30, .6),
  600: Color.fromRGBO(250, 140, 30, .7),
  700: Color.fromRGBO(250, 140, 30, .8),
  800: Color.fromRGBO(250, 140, 30, .9),
  900: Color.fromRGBO(250, 140, 30, 1),
};
const Map<int, Color> pinkColorSwatch = {
  50: Color.fromRGBO(173, 20, 7, .1),
  100: Color.fromRGBO(173, 20, 7, .2),
  200: Color.fromRGBO(173, 20, 7, .3),
  300: Color.fromRGBO(173, 20, 7, .4),
  400: Color.fromRGBO(173, 20, 7, .5),
  500: Color.fromRGBO(173, 20, 7, .6),
  600: Color.fromRGBO(173, 20, 7, .7),
  700: Color.fromRGBO(173, 20, 7, .8),
  800: Color.fromRGBO(173, 20, 7, .9),
  900: Color.fromRGBO(173, 20, 7, 1),
};

class AppColors {
  AppColors._();

  static const Color zazzy = Color(0xFFFA8C1E); //servitora orange
  static const Color gray = Color(0xFFD2D2D2);
  // static const Color blue = Color(0xFF0F75BC);

  static const Color lightColor = Colors.pink;
  static Color lightAccentColor = Colors.pink[50] ?? Colors.pinkAccent;
  static Color lightHighContrastColor = Colors.pink[50] ?? Colors.pinkAccent;
  static Color lightBackgroundColor = Colors.pink[50] ?? Colors.pinkAccent;
  static Color lightScaffoldBackgroundColor = Colors.pink[50] ?? Colors.pinkAccent;
  static const Color lightNavigationBarBackgroundColor = Colors.pink;

  static Color darkColor = Colors.pink[800] ?? Colors.pink;
  static Color darkHighContrastColor = Colors.pink[50] ?? Colors.pinkAccent;
  static Color darkAccentColor = Colors.pink[50] ?? Colors.pinkAccent;
  static const Color darkBackgroundColor = Colors.black;
  static const Color darkScaffoldBackgroundColor = Colors.black;
  static Color darkNavigationBarBackgroundColor = Colors.pink[800] ?? Colors.pink;
}
