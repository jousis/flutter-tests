// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:js_flutter_showcase/theme/colors.dart';

class AppTheme {
  const AppTheme._();

  static Brightness get currentPlatformBrightness => SchedulerBinding.instance.window.platformBrightness;

  static const double maxComponentWidth = 450;

  static OutlineInputBorder defaultInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
  );

  static final lightTheme = ThemeData.light().copyWith(
    cardColor: AppColors.lightAccentColor,
    // visualDensity: VisualDensity.standard,
    primaryColor: AppColors.lightColor,
    // accentColor: AppColors.lightAccentColor,
    // scaffoldBackgroundColor: AppColors.lightScaffoldBackgroundColor,
    cardTheme: const CardTheme(shape: ContinuousRectangleBorder()),
    appBarTheme: const AppBarTheme(
      color: AppColors.lightColor,
    ),
    indicatorColor: AppColors.lightColor,
    tabBarTheme: TabBarTheme(
      labelColor: AppColors.lightHighContrastColor,
      unselectedLabelColor: AppColors.lightHighContrastColor.withAlpha(0xB2),
    ),
    // textTheme: ThemeData.dark().textTheme.copyWith(button: TextStyle(color: AppColors.darkHighContrastColor)),
    // inputDecorationTheme: InputDecorationTheme(
    //   border: defaultInputBorder,
    //   enabledBorder: defaultInputBorder,
    //   focusedBorder: defaultInputBorder,
    // ),
    // elevatedButtonTheme: ElevatedButtonThemeData(
    //   style: ElevatedButton.styleFrom(
    //     // primary: myAppMaterialColor,
    //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    //   ),
    // ),
    // textButtonTheme: TextButtonThemeData(
    //   style: TextButton.styleFrom(
    //     primary: myAppMaterialColor,
    //   ),
    // ),
    // outlinedButtonTheme: OutlinedButtonThemeData(
    //   style: OutlinedButton.styleFrom(
    //     primary: myAppMaterialColor,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(10.0),
    //     ),
    //     side: const BorderSide(width: 1.5, color: myAppMaterialColor),
    //   ),
    // ),
    iconTheme: const IconThemeData(color: AppColors.lightColor),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData().copyWith(
      backgroundColor: AppColors.lightColor,
      // type: BottomNavigationBarType.shifting,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      selectedItemColor: AppColors.lightHighContrastColor,
      unselectedItemColor: AppColors.lightHighContrastColor.withAlpha(0xB2),
      selectedIconTheme: const IconThemeData().copyWith(size: 45),
      unselectedIconTheme: const IconThemeData().copyWith(size: 45),
    ),
    colorScheme: ColorScheme.fromSwatch(primarySwatch: myAppMaterialColor).copyWith(background: AppColors.lightColor),
  );

  static final darkTheme = ThemeData.dark().copyWith(
    cardColor: AppColors.darkColor,
    // visualDensity: VisualDensity.standard,
    primaryColor: AppColors.darkColor,
    dialogBackgroundColor: AppColors.darkBackgroundColor,
    scaffoldBackgroundColor: AppColors.darkScaffoldBackgroundColor,
    cardTheme: const CardTheme(shape: ContinuousRectangleBorder()),
    indicatorColor: AppColors.darkColor,
    tabBarTheme: TabBarTheme(
      labelColor: AppColors.darkHighContrastColor,
      unselectedLabelColor: AppColors.darkHighContrastColor.withAlpha(0xB2),
    ),
    // textTheme: ThemeData.dark().textTheme.copyWith(button: TextStyle(color: AppColors.darkHighContrastColor)),
    // inputDecorationTheme: InputDecorationTheme(
    //   border: defaultInputBorder,
    //   enabledBorder: defaultInputBorder.copyWith(borderSide: BorderSide(color: AppColors.darkHighContrastColor)),
    //   focusedBorder: defaultInputBorder,
    // ),
    // elevatedButtonTheme: ElevatedButtonThemeData(
    //   style: ElevatedButton.styleFrom(
    //     primary: AppColors.darkColor,
    //     onPrimary: AppColors.darkHighContrastColor,
    //     onSurface: AppColors.darkHighContrastColor,
    //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    //   ),
    // ),
    // textButtonTheme: TextButtonThemeData(
    //   style: TextButton.styleFrom(
    //     primary: myAppMaterialColor,
    //   ),
    // ),
    // outlinedButtonTheme: OutlinedButtonThemeData(
    //   style: OutlinedButton.styleFrom(
    //     primary: myAppMaterialColor,
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(10.0),
    //     ),
    //     side: const BorderSide(width: 1.5, color: myAppMaterialColor),
    //   ),
    // ),
    iconTheme: IconThemeData(color: AppColors.darkHighContrastColor),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData().copyWith(
      backgroundColor: AppColors.darkNavigationBarBackgroundColor,
      // type: BottomNavigationBarType.shifting,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      selectedItemColor: AppColors.darkHighContrastColor,
      unselectedItemColor: AppColors.darkAccentColor,
      selectedIconTheme: const IconThemeData().copyWith(size: 45),
      unselectedIconTheme: const IconThemeData().copyWith(size: 45),
    ),
    colorScheme: ColorScheme.fromSwatch(primarySwatch: myAppMaterialColor).copyWith(background: AppColors.darkBackgroundColor),
  );

  static void setStatusBarAndNavBarColors(ThemeMode themeMode) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: themeMode == ThemeMode.light ? Brightness.dark : Brightness.light,
        systemNavigationBarIconBrightness: themeMode == ThemeMode.light ? Brightness.light : Brightness.light,
        systemNavigationBarColor: themeMode == ThemeMode.light
            ? AppColors.lightNavigationBarBackgroundColor
            : AppColors.darkNavigationBarBackgroundColor,
        systemNavigationBarDividerColor: Colors.transparent,
      ),
    );
  }
}
