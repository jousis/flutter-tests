import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/settings/settings.dart';

@RoutePage()
class FloorPlanAltPage extends StatefulWidget {
  @override
  _FloorPlanAltPageState createState() => _FloorPlanAltPageState();
}

class _FloorPlanAltPageState extends State<FloorPlanAltPage> {
  final _stackKey = GlobalKey();

  List<FloorPlanTable> tables = const [
    FloorPlanTable(
      id: '1',
      name: 'Τραπέζι 1',
      xPos: 0.25,
      yPos: 0.02,
      width: 0.3,
      height: 0.1,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '2',
      name: 'Τραπέζι 2',
      xPos: 0.02,
      yPos: 0.17,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '3',
      name: 'Τραπέζι 3',
      xPos: 0.02,
      yPos: 0.30,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '4',
      name: 'Τραπέζι 4',
      xPos: 0.02,
      yPos: 0.41,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '5',
      name: 'Τραπέζι 5',
      xPos: 0.02,
      yPos: 0.53,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '6',
      name: 'Τραπέζι 6',
      xPos: 0.02,
      yPos: 0.63,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    FloorPlanTable(
      id: '7',
      name: 'Τραπέζι 7',
      xPos: 0.02,
      yPos: 0.84,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
  ];

  double _ratio = 1;
  double _top = 0;
  double _left = 0;

  double buttonTop = 0.01;
  double buttonLeft = 0.26;
  double buttonWidth = 0.3;
  double buttonHeight = 0.1;

  void _zoomIn() {
    setState(() {
      _ratio += 0.1;
    });
  }

  void _zoomOut() {
    setState(() {
      _ratio -= 0.1;
    });
  }

  void _resetZoom() {
    setState(() {
      _ratio = 1;
      _left = 0;
      _top = 0;
    });
  }

  void _panUpdate(DragUpdateDetails details) {
    setState(() {
      _top = _top + details.delta.dy;
      _left = _left + details.delta.dx;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(icon: const Icon(Icons.restore), onPressed: _resetZoom),
          IconButton(icon: const Icon(Icons.zoom_out), onPressed: _zoomOut),
          IconButton(icon: const Icon(Icons.zoom_in), onPressed: _zoomIn),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FractionallySizedBox(
            widthFactor: _ratio,
            heightFactor: _ratio,
            child: LayoutBuilder(
              builder: (context, constraints) {
                final maxDimensionConstraint =
                    constraints.maxWidth > constraints.maxHeight ? constraints.maxHeight : constraints.maxWidth;
                return Container(
                  constraints: BoxConstraints(
                    maxWidth: maxDimensionConstraint,
                    maxHeight: maxDimensionConstraint,
                  ),
                  child: GestureDetector(
                    onPanUpdate: _panUpdate,
                    child: Stack(
                      key: _stackKey,
                      alignment: Alignment.center,
                      children: [
                        Image.asset(
                          'assets/floor_plans/custom_plan_02.png',
                          fit: BoxFit.contain,
                        ),
                        ...tables.map(
                          (table) => Align(
                            alignment: FractionalOffset(table.xPos, table.yPos),
                            // top: _top + (table.yPos * maxDimensionConstraint * _ratio),
                            // left: _left + (table.xPos * maxDimensionConstraint * _ratio),
                            // width: table.width * maxDimensionConstraint * _ratio,
                            // height: table.height * maxDimensionConstraint * _ratio,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                  EdgeInsets.zero,
                                ),
                              ),
                              onPressed: () => {},
                              child: FittedBox(
                                child: Text(table.name),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
