// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/settings/settings.dart';

class FloorPlanTableWidget extends StatefulWidget {
  const FloorPlanTableWidget({
    super.key,
    required this.table,
  });

  final FloorPlanTable table;

  @override
  State<FloorPlanTableWidget> createState() => _FloorPlanTableWidgetState();
}

class _FloorPlanTableWidgetState extends State<FloorPlanTableWidget> {
  @override
  Widget build(BuildContext context) {
    final table = widget.table;
    return Card(
      color: Color(table.backgroundColor),
      // style: ButtonStyle(
      //   padding: MaterialStateProperty.all(
      //     EdgeInsets.zero,
      //   ),
      // ),
      // onPressed: () => {},
      child: FittedBox(
        child: Text(table.name),
      ),
    );
  }
}
