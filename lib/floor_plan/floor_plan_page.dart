import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:js_flutter_showcase/floor_plan/table.dart';
import 'package:js_flutter_showcase/settings/settings.dart';

@RoutePage()
class FloorPlanPage extends StatefulWidget {
  @override
  _FloorPlanPageState createState() => _FloorPlanPageState();
}

class _FloorPlanPageState extends State<FloorPlanPage> {
  final _stackKey = GlobalKey();

  List<FloorPlanTable> tables = [
    const FloorPlanTable(
      id: '1',
      name: 'Τραπέζι 1',
      xPos: 0.25,
      yPos: 0.02,
      width: 0.3,
      height: 0.1,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '2',
      name: 'Τραπέζι 2',
      xPos: 0.02,
      yPos: 0.17,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '3',
      name: 'Τραπέζι 3',
      xPos: 0.02,
      yPos: 0.30,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '4',
      name: 'Τραπέζι 4',
      xPos: 0.02,
      yPos: 0.41,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '5',
      name: 'Τραπέζι 5',
      xPos: 0.02,
      yPos: 0.53,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '6',
      name: 'Τραπέζι 6',
      xPos: 0.02,
      yPos: 0.63,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
    const FloorPlanTable(
      id: '7',
      name: 'Τραπέζι 7',
      xPos: 0.02,
      yPos: 0.84,
      width: 0.15,
      height: 0.07,
      capacity: 5,
      backgroundColor: 0xFFABABAB,
    ),
  ];

  double _ratio = 1;
  double _top = 0;
  double _left = 0;

  double buttonTop = 0.01;
  double buttonLeft = 0.26;
  double buttonWidth = 0.3;
  double buttonHeight = 0.1;

  bool panTablesMode = false;
  Offset? panStartGlobalPosition;
  double? initialTablePanXPos;
  double? initialTablePanYPos;

  bool resizeTableMode = false;
  Offset? resizeStartGlobalPosition;
  double? initialTableWidth;
  double? initialTableHeight;

  void _zoomIn() {
    setState(() {
      _ratio += 0.1;
    });
  }

  void _zoomOut() {
    setState(() {
      _ratio -= 0.1;
    });
  }

  void _resetZoom() {
    setState(() {
      _ratio = 1;
      _left = 0;
      _top = 0;
    });
  }

  void _panUpdate(DragUpdateDetails details) {
    setState(() {
      _top = _top + details.delta.dy;
      _left = _left + details.delta.dx;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(icon: const Icon(Icons.restore), onPressed: _resetZoom),
          IconButton(icon: const Icon(Icons.zoom_out), onPressed: _zoomOut),
          IconButton(icon: const Icon(Icons.zoom_in), onPressed: _zoomIn),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              final maxDimension =
                  constraints.maxWidth > constraints.maxHeight ? constraints.maxHeight * 0.9 : constraints.maxWidth * 0.9;
              // print('maxWidth : ${constraints.maxWidth}');
              // print('maxHeight : ${constraints.maxHeight}');
              // print('maxDimensionConstraint : $maxDimensionConstraint');
              // ignore: avoid_unnecessary_containers
              return Container(
                // constraints: BoxConstraints(
                //     maxWidth: constraints.maxWidth,
                //     // maxHeight: constraints.maxHeight,
                //     ),
                child: GestureDetector(
                  // onDoubleTap: () {
                  //   print('resize mode');
                  //   setState(() {
                  //     resizeTableMode = !resizeTableMode;
                  //   });
                  // },
                  onPanUpdate: _panUpdate,
                  child: Stack(
                    key: _stackKey,
                    alignment: Alignment.center,
                    children: [
                      Positioned(
                        top: _top,
                        left: _left,
                        width: maxDimension * _ratio,
                        // height: constraints.maxHeight * _ratio,
                        // width: double.infinity,
                        child: Image.asset(
                          'assets/floor_plans/custom_plan_02.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      // ...tables.map(
                      //   (e) => Container(
                      //     child: Positioned(
                      //       top: _top + (e.yPos * maxDimension * _ratio),
                      //       left: _left + (e.xPos * maxDimension * _ratio),
                      //       width: e.width * maxDimension * _ratio,
                      //       height: e.height * maxDimension * _ratio,
                      //       child: GestureDetector(
                      //         child: FloorPlanTableWidget(
                      //           table: e,
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      ...tables.indexed.map(
                        (tableIndexed) => Positioned(
                          top: _top + (tableIndexed.$2.yPos * maxDimension * _ratio),
                          left: _left + (tableIndexed.$2.xPos * maxDimension * _ratio),
                          width: tableIndexed.$2.width * maxDimension * _ratio,
                          height: tableIndexed.$2.height * maxDimension * _ratio,
                          child: GestureDetector(
                            onPanStart: (details) {
                              if (resizeTableMode) return;
                              setState(() {
                                panTablesMode = true;
                                panStartGlobalPosition = details.globalPosition;
                                initialTablePanXPos = tableIndexed.$2.xPos;
                                initialTablePanYPos = tableIndexed.$2.yPos;
                              });
                            },
                            onPanEnd: (details) {
                              setState(() {
                                panTablesMode = false;
                                panStartGlobalPosition = null;
                                initialTablePanXPos = null;
                                initialTablePanYPos = null;
                              });
                            },
                            onPanCancel: () {
                              setState(() {
                                panTablesMode = false;
                                panStartGlobalPosition = null;
                                initialTablePanXPos = null;
                                initialTablePanYPos = null;
                              });
                            },
                            onPanUpdate: (details) {
                              if (resizeTableMode) return;
                              final startPos = panStartGlobalPosition;
                              final initialX = initialTablePanXPos;
                              final initialY = initialTablePanYPos;
                              if (startPos == null || initialX == null || initialY == null) return;
                              final newDeltaX = details.globalPosition.dx - startPos.dx;
                              final newDeltaY = details.globalPosition.dy - startPos.dy;
                              final newX = initialX + (newDeltaX / (maxDimension * _ratio));
                              final newY = initialY + (newDeltaY / (maxDimension * _ratio));
                              final newTable = tableIndexed.$2.copyWith(
                                xPos: newX,
                                yPos: newY,
                              );
                              setState(() {
                                tables = tables.map(
                                  (e) {
                                    if (e.id != newTable.id) {
                                      return e;
                                    }
                                    return newTable;
                                  },
                                ).toList();
                              });
                            },
                            child: Stack(
                              fit: StackFit.expand,
                              children: [
                                FloorPlanTableWidget(
                                  table: tableIndexed.$2,
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: GestureDetector(
                                    onPanStart: (details) {
                                      setState(() {
                                        resizeTableMode = true;
                                        resizeStartGlobalPosition = details.globalPosition;
                                        initialTableWidth = tableIndexed.$2.width;
                                        initialTableHeight = tableIndexed.$2.height;
                                      });
                                    },
                                    onPanEnd: (details) {
                                      setState(() {
                                        resizeTableMode = false;
                                      });
                                    },
                                    onPanCancel: () {
                                      setState(() {
                                        resizeTableMode = false;
                                      });
                                    },
                                    onPanUpdate: (details) {
                                      if (!resizeTableMode) return;
                                      final startPos = resizeStartGlobalPosition;
                                      final initialW = initialTableWidth;
                                      final initialH = initialTableHeight;
                                      if (startPos == null || initialW == null || initialH == null) return;
                                      final newDeltaX = details.globalPosition.dx - startPos.dx;
                                      final newDeltaY = details.globalPosition.dy - startPos.dy;
                                      final newW = initialW + (newDeltaX / (maxDimension * _ratio));
                                      final newH = initialH + (newDeltaY / (maxDimension * _ratio));
                                      final newTable = tableIndexed.$2.copyWith(
                                        width: newW,
                                        height: newH,
                                      );
                                      setState(() {
                                        tables = tables.map(
                                          (e) {
                                            if (e.id != newTable.id) {
                                              return e;
                                            }
                                            return newTable;
                                          },
                                        ).toList();
                                      });
                                    },
                                    child: Container(
                                      width: 10,
                                      height: 10,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
